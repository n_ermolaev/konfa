from django.contrib import admin
from .models import News

class NewsAdmin(admin.ModelAdmin):
    search_fields = ['title']

admin.site.register(News, NewsAdmin)
# Register your models here.
