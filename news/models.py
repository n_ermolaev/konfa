from django.db import models

class News(models.Model):
	title = models.CharField(max_length=70)
	text = models.TextField()
	is_public = models.BooleanField()
	
	def __str__(self):
		return '%s' % (self.title)