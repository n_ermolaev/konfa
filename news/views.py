from django.shortcuts import render, render_to_response
from .models import News
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse

def news(request):
	news_list = News.objects.all()
	paginator = Paginator(news_list, 1)
	page = request.GET.get('page')
	try:
		news_pag = paginator.page(page)
	except PageNotAnInteger:
		news_pag = paginator.page(1)
	return render_to_response('news/base_news.html', {'news': news_pag})