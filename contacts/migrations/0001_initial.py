# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contacts',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('whois', models.CharField(max_length=70)),
                ('name', models.CharField(max_length=70)),
                ('number', models.IntegerField()),
                ('email', models.EmailField(max_length=75)),
            ],
        ),
    ]
