from django.shortcuts import render
from .models import Contacts

def index(request):
	contacts_list = Contacts.objects.all()
	context = {'contacts': contacts_list}
	return render(request, 'contacts/base_contacts.html', context)
# Create your views here.
