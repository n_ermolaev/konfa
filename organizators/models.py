from django.db import models

class Organizators(models.Model):
	name = models.CharField(max_length=70)
	profession = models.CharField(max_length=150)
	
	def __str__(self):
		return '%s' % (self.name)
