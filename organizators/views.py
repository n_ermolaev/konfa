from django.shortcuts import render
from .models import Organizators

def index(request):
	organizators_list = Organizators.objects.all()
	context = {'organizators': organizators_list}
	return render(request, 'organizators/base_organizators.html', context)
# Create your views here.
