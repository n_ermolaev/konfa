from django.conf.urls import patterns, url
from organizators import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
)
