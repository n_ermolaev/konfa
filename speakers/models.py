from django.db import models

class Speakers(models.Model):
	speaker = models.CharField(max_length=70)
	opisanie = models.TextField()
	
	def __str__(self):
		return '%s' % (self.speaker)