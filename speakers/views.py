from django.shortcuts import render
from .models import Speakers

def index(request):
	speakers_list = Speakers.objects.all()
	context = {'speakers': speakers_list}
	return render(request, 'speakers/base_speakers.html', context)
# Create your views here.
