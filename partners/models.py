from django.db import models

class Partners(models.Model):
	image = models.ImageField(upload_to="partners/img", null=True, blank=True)
	text = models.TextField()
# Create your models here.
