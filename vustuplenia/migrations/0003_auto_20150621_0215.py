# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('vustuplenia', '0002_vustuplenia_is_public'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='vustuplenia',
            name='is_public',
        ),
        migrations.AddField(
            model_name='vustuplenia',
            name='public',
            field=models.BooleanField(default=False),
        ),
    ]
