from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Vustuplenia
from .forms import VustupleniaForm

def index(request):
	vustuplenia_list = Vustuplenia.objects.all()
	context = {'vustuplenia': vustuplenia_list}
	return render(request, 'vustuplenia/base_vustuplenia.html', context)

def edit_vustuplenia(request):
    context = {'vustuplenia': Vustuplenia.objects.all()}
    return render(request, 'vustuplenia/edit.html', context)
	
def add_vustuplenia(request):
	if request.POST:
		form = VustupleniaForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/vustuplenia/edit')
		else:
			context = {'form': form, 'vustuplenia': Vustuplenia.objects.all()}
			return render(request, 'vustuplenia/add.html', context)
	else:
		form = VustupleniaForm()
		context = {'form': form, 'vustuplenia': Vustuplenia.objects.all()}
		return render(request, 'vustuplenia/add.html', context)

def change_vustuplenia(request, id):
	vustuplenia = Vustuplenia.objects.get(pk=id)
	if request.POST:
		form = VustupleniaForm(request.POST, instance=vustuplenia)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/vustuplenia/edit')
		else:
			context = {'form': form, 'vustuplenia': Vustuplenia.objects.all()}
			return render(request, 'vustuplenia/change.html', context)
	else:
		form = VustupleniaForm(instance=vustuplenia)
		context = {'form': form, 'vustuplenia': Vustuplenia.objects.all()}
		return render(request, 'vustuplenia/change.html', context)

def delete_vustuplenia(request, id):
   dead = Vustuplenia.objects.get(pk=id)
   dead.delete()
   return HttpResponseRedirect('/vustuplenia/edit')