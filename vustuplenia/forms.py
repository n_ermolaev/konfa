from django import forms
from django.forms import ModelForm
from .models import Vustuplenia

class NameForm(forms.Form):
	dictor = forms.CharField(label='Диктор', max_length=70)
	theme = forms.CharField(label='Тема', max_length=70)
	public = forms.BooleanField(label='Выступал', initial=False)

class VustupleniaForm(ModelForm):
	class Meta:
		model = Vustuplenia
		fields = ['dictor', 'theme', 'public']