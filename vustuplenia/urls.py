from django.contrib import admin
from django.conf.urls import patterns, url
from vustuplenia import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^edit/$', views.edit_vustuplenia, name='edit'),
	url(r'^edit/add/$', views.add_vustuplenia, name='add'),
	url(r'^edit/change/(?P<id>\d{1,9})/$', views.change_vustuplenia, name='change'),
	url(r'^edit/delete/(?P<id>\d{1,9})/$', views.delete_vustuplenia, name='delete'),
)
