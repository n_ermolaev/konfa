from django.db import models

class Vustuplenia(models.Model):
	dictor = models.CharField(max_length=70)
	theme = models.CharField(max_length=70)
	public = models.BooleanField(default=False)
	
	def __str__(self):
		return '%s' % (self.theme)