from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.views.generic import TemplateView

urlpatterns =  patterns('',
	url(r'^organizators/', include('organizators.urls')),
	url(r'^news/', include('news.urls')),
	url(r'^partners/', include('partners.urls')),
	url(r'^vustuplenia/', include('vustuplenia.urls')),
	url(r'^contacts/', include('contacts.urls')),
	url(r'^speakers/', include('speakers.urls')),
	url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:    
    urlpatterns += patterns('', 
    url(r'^404$', TemplateView.as_view(template_name='404.html')),
    url(r'^500$', TemplateView.as_view(template_name='500.html')),)